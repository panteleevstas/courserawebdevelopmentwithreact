import {Breadcrumb, BreadcrumbItem, Button, Card, CardBody, CardImg, CardText, CardTitle, Row} from "reactstrap";
import {Link} from "react-router-dom";
import {Col, Label, Modal, ModalBody, ModalHeader} from "reactstrap";
import {Component} from "react/cjs/react.production.min";
import {Control, Errors, LocalForm} from "react-redux-form";
import {Loading} from './LoadingComponent';


class CommentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            author: '',
            comment: '',
            rating: 0,
            touched: {
                author: '',
                comment: '',
                rating: 0
            }
        }
    }

    toggleModal = () => {
        this.setState({isModalOpen: !this.state.isModalOpen})
    };

    handleSubmit = (values) => {
        this.toggleModal();
        this.props.addComment(this.props.dishId, values.rating, values.author, values.comment);
    }

    render() {
        const required = (val) => val && val.length;
        const maxLength = (len) => (val) => !(val) || (val.length <= len);
        const minLength = (len) => (val) => val && (val.length >= len);

        return (
            <div>
                <Button outline onClick={this.toggleModal}><span className="fa fa-comment fa-lg"></span>Submit
                    Comments</Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit comment</ModalHeader>
                    <ModalBody>
                        <div className="row row-content">
                            <div className="col-12">
                                <h3>Send us your Feedback</h3>
                            </div>
                            <div className="col-12 col-md-9">
                                <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                                    <Row className="form-group">
                                        <Label htmlFor="rating" md={2}>Last Name</Label>
                                        <Col md={10}>
                                            <Control.select model=".rating" id="rating" name="rating"

                                                            placeholder="Rating"
                                                            className="form-control"
                                            >
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </Control.select>
                                        </Col>
                                    </Row>
                                    <Row className="form-group">
                                        <Label htmlFor="author" md={2}>author</Label>
                                        <Col md={10}>
                                            <Control.text model=".author" id="author" name="author"
                                                          placeholder="Author"
                                                          className="form-control"
                                                          validators={{
                                                              minLength: minLength(3),
                                                              maxLength: maxLength(15)
                                                          }}
                                            />
                                            <Errors
                                                className="text-danger"
                                                model=".author"
                                                show="touched"
                                                messages={{
                                                    required: 'Required',
                                                    minLength: 'Must be greater than 3 characters',
                                                    maxLength: 'Must be 15 characters or less'
                                                }}
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="form-group">
                                        <Label htmlFor="comment" md={2}>Comments</Label>
                                        <Col md={10}>
                                            <Control.textarea model=".comment" id="comment" name="comment"
                                                              rows="3"
                                                              placeholder="Comment"
                                                              className="form-control"
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="form-group">
                                        <Col md={{size: 10, offset: 2}}>
                                            <Button type="submit" color="primary">
                                                Send Feedback
                                            </Button>
                                        </Col>
                                    </Row>
                                </LocalForm>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>
            </div>
        );
    }

}


function RenderDish({dish}) {

    if (dish != null) {
        return (
            <Card>
                <CardImg top src={dish.image} alt={dish.name}/>
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        );
    } else
        return <div>hi</div>;
}


function RenderComments({comments, addComment, dishId}) {

    if (!comments) {
        return <div>empty</div>
    }

    const items = comments.map((comment) => <li key={comment.id} style={{textAlign: "left"}}>
        <p>{comment.comment}</p>
        <p>--- {comment.author} {new Date(comment.date).toLocaleTimeString()}</p>
    </li>)
    return (
        <div>
            <h4>Comments</h4>
            <ul>{items}</ul>
            <CommentForm dishId={dishId} addComment={addComment}/>
        </div>
    );
}


const DishDetail = (props) => {

    if (props.isLoading) {
        return (
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        );
    } else if (props.errMess) {
        return (
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        );
    } else if (props.dish != null) {
        return (
            <div>
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb>

                <div className="row">
                    <div className="col-12 col-md-3 ml-3">
                        <RenderDish dish={props.dish}/>
                    </div>
                    <div className="col-md-5">
                        <RenderComments comments={props.comments}
                                        addComment={props.addComment}
                                        dishId={props.dish.id}
                        />
                    </div>
                </div>
            </div>
        );
    }

}


export default DishDetail;